package com.oreillyauto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.oreillyauto.java.helpers.StringHelper;

public class TestStringHelper {

	@Test
	public void testStringHelper() {
		String result = "";
		
		result = testReverse("");
		assertEquals(result, null);
		
		result = testReverse(null);
		assertEquals(result, null);
		
		result = testReverse("Test");
		assertEquals(result, "tseT");
	}

	private String testReverse(String string) {
		return StringHelper.reverseString(string);
	}

}
