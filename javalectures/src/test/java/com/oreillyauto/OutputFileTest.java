package com.oreillyauto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class OutputFileTest {
	private static final String HELLO_WORLD = "Hello World";
	private File file;

	@Before
	public void createOutputFile() {
		// Java 8 try with resources
		file = new File("some_unique_file.txt");
		String str = HELLO_WORLD;
		
		try (FileWriter fw = new FileWriter(file.getAbsolutePath()); 
				BufferedWriter writer = new BufferedWriter(fw);) {
			
			writer.write(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    @Test
    public void testFile1() {
        // code for test case objective
        boolean fileExists = file.exists();
        assertTrue(fileExists);
        
        boolean fooExists = new File("foo.txt").exists();
        assertFalse(fooExists);
    }

    @Test
    public void testFile2() {
        // code for test case objective
        String contents = "";
                
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            
            while ((line = br.readLine()) != null) {              
            	contents = line;                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        assertTrue(HELLO_WORLD.equalsIgnoreCase(contents));
    }

}
