package com.oreillyauto.java.week2.classes;

public class ThreadThatImplements implements Runnable {

	@Override
	public void run() {
		try {
			// Displaying the thread that is running
			System.out.println("Thread " + Thread.currentThread().getId() + " is running");
		} catch (Exception e) {
			// Throwing an exception
			System.out.println("Exception is caught");
		}
		
	}
    
//    public void run() {
//        try {        	
//            // Displaying the thread that is running
//            System.out.println("Thread " + Thread.currentThread().getId() + " is running");
//        } catch (Exception e) {
//            // Throwing an exception
//            System.out.println("Exception is caught");
//        }
//    }
}
