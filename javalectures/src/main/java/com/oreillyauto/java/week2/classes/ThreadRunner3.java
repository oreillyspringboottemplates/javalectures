package com.oreillyauto.java.week2.classes;

public class ThreadRunner3 {

    public ThreadRunner3() {
        //testMultithreadingDemo();
        testMultithreadingDemo2();
//        testExampleThread();
    }

    private void testExampleThread() {
        // The second argument is a delay between successive outputs.  The delay is 
        // measured in milliseconds.  "10", for instance, means, "print a line every
        // hundredth of a second".        
        ExampleThread mt = new ExampleThread("A", 31);
        ExampleThread mt2 = new ExampleThread("B", 25);
        ExampleThread mt3 = new ExampleThread("C", 10);
        mt.start();
        mt2.start();
        mt3.start();

        try {
            System.out.println("Working�");
            mt.join();
            mt2.join();
            mt3.join();
            System.out.println("Done");
        }
        catch (InterruptedException ie) {
            System.out.println("Interrupted Exception!: " + ie.getMessage());
        }
    }

    private void testMultithreadingDemo2() {
        System.out.println("Running threads...");
        int n = 8; // Number of threads
        Object[] threadPool = new Object[8];
        
        for (int i=0; i<8; i++) {
            Thread object = new Thread(new MultithreadingDemo2());
            threadPool[i] = object;
            object.start();
        }
        
        for (int i=0; i<8; i++) {
            Thread t = (Thread)threadPool[i];
            
            try {
                t.join();    
            } catch (Exception e) {
                System.out.println("uh oh! " + e.getMessage());
            }
        }
        
        System.out.println("Done.");
        
    }

    private void testMultithreadingDemo() {
        // Test 
        int n = 8; // Number of threads

        for (int i = 0; i < 8; i++) {
            MultithreadingDemo object = new MultithreadingDemo();
            object.start();
        }
    }

    public static void main(String[] args) {
        new ThreadRunner3();
    }

}
