package com.oreillyauto.java.week2.classes;

public class ThreadRunner2 {
    public ThreadRunner2() {
        for (int i = 0; i < 8; i++) {
        	new Thread(new ThreadThatImplements()).start();
        }
    }

    public static void main(String ...args) {
        new ThreadRunner2();        
    }
}
