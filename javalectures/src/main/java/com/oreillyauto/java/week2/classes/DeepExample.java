package com.oreillyauto.java.week2.classes;

import java.util.Arrays;

public class DeepExample {     
    private int[] data;
 
    // Altered to make a deep copy of values
    public DeepExample(int[] values) {
        data = new int[values.length];
        
        for (int i = 0; i < values.length; i++) {
            data[i] = values[i];
        }
        
        Arrays.copyOf(values, values.length);
    }
 
    public int getValue(int position) {
    	return data[position];
    }
    
    public void showData() {
        System.out.println(Arrays.toString(data));
    }
}
