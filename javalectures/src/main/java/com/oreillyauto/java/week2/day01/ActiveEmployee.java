package com.oreillyauto.java.week2.day01;

// Example 5
public class ActiveEmployee {
	
	public static int employeeCount = 0;
	
    public final void getStatus() {
        System.out.println("Active");
    }
    
    public int getEmployeeCount() {
    	return employeeCount;
    }
    
    public static String getClassName() {
    	return ActiveEmployee.class.getSimpleName();
    }
}
