package com.oreillyauto.java.week2.day01;

import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.java.helpers.Helper;

public class Week02Day01 {

	public final static Double PI = 3.14159D;  // Example 2
//	public final static Double PI2;            // Example 2 The blank final field PI2 may not have been initialized
	public final Double PI3 = null;
	final static List<CarPart> partList = new ArrayList<CarPart>();
			
	public Week02Day01(int age) {
//		testFinal();                             // Example 1
//		testFinalMessage(age);                   // Example 4
//		testFinalList();                         // Example 8
//		testFinal2(PI);                          // Example 9
//		testFinalMemberVariable();               // Example 10
//		testIncrementingStaticMemberVariables(); // Example 11
//		testStaticMethod();                      // Example 12
//		testStaticClassesAndMethods();           // Example 13
//		                                         /* Example 14 review MyClass1.java */
//		testArrayLists();                        // Example 15                             
//		testArrayListCapacity();                 // Example 16
//		testArrayListDefaultCapacity();          // Example 17
//		testReduceMemoryFootprint();             // Example 18
	}

	private void testFinal() {
//		PI = 3.1415; // The final field Week02Day01.PI cannot be assigned
	}
	
	private void testFinalMessage(int age) {
		final String message; // <= declared final; not initialized	
		
		if (age > 16) {
			message = "Available movie types: G, PG, PG-13, R";
		} else {
			message = "Available movie types: G, PG, PG-13";
		}
		
		System.out.println(message);	
	}

	private void testReduceMemoryFootprint() {
		/** We are using the testArrayListDefaultCapacity to set this up... */
        ArrayList<Object> testList = new ArrayList<Object>(); 
        System.out.println("testList.size() => " + testList.size());
        
        int capacity = Helper.getArrayListCapacity(testList);
        System.out.println("testList capacity => " + capacity);
        
        // What happens to capacity if we add 10 items?
        System.out.println("Adding 10 items to the list...");
        
        for (int i = 0; i < 12; i++) {
            testList.add(i);
        }
        
        capacity = Helper.getArrayListCapacity(testList);
        System.out.println("testList capacity (size="+testList.size()+") => " + capacity);
        
        /** Now we can reduce our memory footprint... */
        System.out.println("Trimming capacity to size...");
        testList.trimToSize();
        capacity = Helper.getArrayListCapacity(testList);
        System.out.println("testList capacity (size="+testList.size()+") => " + capacity);
	}


	private void testArrayListDefaultCapacity() {
        ArrayList<Object> myList = new ArrayList<Object>();
        int capacity = Helper.getArrayListCapacity(myList);
//        System.out.println("testList capacity => " + capacity);
//        
//        myList.add(1);
//        capacity = Helper.getArrayListCapacity(myList);
//        System.out.println("testList capacity with 1 element => " + capacity);
//
//        int start = 0;
//        int stop = 11;
//        ArrayList<Object> testList = new ArrayList<Object>();
//        System.out.println("Adding "+(stop-start)+" items to the test list...");
//
//        for (int i = start; i < stop; i++) {
//            testList.add(i); // or testList.set(i, i);
//        }
//                
//        capacity = Helper.getArrayListCapacity(testList);
//        System.out.println("testList capacity () => " + capacity);
        
        // Capacity = 
        //   IF(NEW_CAPACITY > OLD_CAPACITY 
        //      THEN (OLD_CAPACITY * 3) / 2 )
        //   ELSE 
        //      OLD_CAPACITY
        // (Round down)
        
        // What is the capacity for 11 elements?
        // What is the capacity for 16 elements?
        
	}
	
	private void testArrayListCapacity() {
		ArrayList<Object> testList = new ArrayList<Object>(100); // testList has a capacity of 100.
		System.out.println("testList.size() => " + testList.size());
		
		int capacity = Helper.getArrayListCapacity(testList);
		System.out.println("testList capacity => " + capacity);
	}

	// Recap of Arraylists
	private void testArrayLists() {
		List<String> myArrayList = new ArrayList<String>();
		myArrayList.add("StringA");
		myArrayList.add("StringB");
		System.out.println(myArrayList.get(1)); // StringB?

		// Test index out of scope/bounds
		System.out.println(myArrayList.get(2)); // java.lang.IndexOutOfBoundsException
	}

	/**
	 * We can access a static method inside of a static innerclass
	 */
	private void testStaticClassesAndMethods() {	
		System.out.println(Helper.FooHelper.getFoo());
	}

	private void testStaticMethod() {
		// Accessing a static method directly in a class  
		System.out.println(ActiveEmployee.getClassName());
	}

	private void testIncrementingStaticMemberVariables() {
		ActiveEmployee susan = new ActiveEmployee();
		ActiveEmployee.employeeCount++;
		ActiveEmployee ryan = new ActiveEmployee();
		ActiveEmployee.employeeCount++;

		System.out.println("susan.employeeCount:"+ susan.employeeCount + " "
				+ "ryan.employeeCount:" + ryan.employeeCount);
		
		// Access the static member directly from the instance (wrong way)
		if (susan.employeeCount == ryan.employeeCount) {
			System.out.println("Samo, samo");
		}

		// Access the static member ... statically (right way)
		if (susan.getEmployeeCount() == ryan.getEmployeeCount()) {
			System.out.println("Static member variables are shared between instances.");
		} else {
			System.out.println("Static member variables are NOT shared between instances.");
		}
	}

	private void testFinalMemberVariable() {
		// We do not need to instantiate the teamnet
		// class to access static member variables
		System.out.println(Teamnet.CLAZZ);
	}

	private void testFinalList() {
		// Can we add new values to the final static list
		partList.add(new CarPart(123456, "OilPan123456", "Oil Pan"));
		partList.add(new CarPart(123457, "OilPan123457", "Large Oil Pan"));
		Helper.printWildList(partList);
	}

	private void testFinal2(double PI) {
		PI = 3.14; // Re-assign the value for this variable
		System.out.println("PI=" + PI);
	}

	public static void main(String[] args) {
	    //System.out.println(foo);
		final int age;         // Example 3
		age = 17;
		new Week02Day01(age);
	}

}
