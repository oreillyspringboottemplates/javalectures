package com.oreillyauto.java.week2.classes;
public class TipTop
{
  static final Integer i1 = 1;
  final Integer i2 = 2;
  Integer i3 = 3;

  public void foo() {
	  class Whatever {
		  
	  }
	  
	  Whatever w = new Whatever();
  }
  
  public static void main(String[] args)
  {
    final Integer i4 = 4;
    Integer i5 = 5;

    class Inner 
    {
       final Integer i6 = 6;
       Integer i7 = 7;

       Inner()
       {
          System.out.print(i6 + i7);
       }
    }
  }
}