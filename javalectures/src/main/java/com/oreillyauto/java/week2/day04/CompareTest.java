package com.oreillyauto.java.week2.day04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gson.Gson;

/**
 * In this class, we will build a multilevel comparator that
 * compares multiple fields. We will sort by first name and 
 * then by last name.
 * 
 * Realize that "Comparable" does not allow sorting over more
 * than one field. We need to implement Comparator to 
 * accomplish this.
 */
public class CompareTest {
	
	public CompareTest() {
		// Build a list and populate it 
		List<Student> studentList = new ArrayList<>();
		Student student1 = new Student("Joe", "Dirt");
		Student student2 = new Student("Joe", "Crabshack");
		studentList.add(student1);
		studentList.add(student2);
	
		/**
		 * Multilevel Comparator
		 * Sort on First Name
		 * Then sort on Last Name
		 */
		Collections.sort(studentList, new Comparator<Student>() {
			@Override
			public int compare(Student student1, Student student2) {
				String firstName1 = ((Student) student1).getFirstName();
				String firstName2 = ((Student) student2).getFirstName();
				int sComp = firstName1.compareTo(firstName2);

				if (sComp != 0) {
					return sComp;
				}

				String lastName1 = ((Student) student1).getLastName();
				String lastName2 = ((Student) student2).getLastName();
				return lastName1.compareTo(lastName2);
			}
		});
		
		// Print the list of students in ASCENDING order.
		// Joe Crabshack should print BEFORE Joe Dirt.
		System.out.println(studentList);		
		
	}

	public static void main(String[] args) {
		new CompareTest();
	}
	
	class Student {
		private String firstName;
		private String lastName;

		public Student(String firstName, String lastName) {
			super();
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		@Override
		public String toString() {
			return new Gson().toJson(this);
		}
	}
	
}
