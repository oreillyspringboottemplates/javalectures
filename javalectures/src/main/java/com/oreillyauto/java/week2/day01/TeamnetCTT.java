package com.oreillyauto.java.week2.day01;

// Example 7
// Cannot extend a *final* class
// The type TeamnetCTT cannot subclass the final class Teamnet
public class TeamnetCTT /*extends Teamnet*/ { 
    public TeamnetCTT() {
    }
}
