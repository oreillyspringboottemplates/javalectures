package com.oreillyauto.java.week2.classes;

public class ThreadExample {

    public ThreadExample() throws Exception {
        System.out.println("Start...");
        long start = System.currentTimeMillis();
        int threadCount = 8;
        Thread[] threadPool = new Thread[threadCount];
        
        for (int i = 0; i < threadCount; i++) {
            threadPool[i] = new MyThread(("Thread " + i), (i+1));    
        }
        
        for (int i = 0; i < threadCount; i++) {
            threadPool[i].start();
            threadPool[i].join();
        }
        long end = System.currentTimeMillis();
        System.out.println("Done.");
        
        System.out.println("Finished in " + (end-start) + " milliseconds");
    }

    public static void main(String[] args) {
        try {
            new ThreadExample();    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    class MyThread extends Thread {
        private int count;
        private String name;
        
        public MyThread(String name, int count) {
            this.name = name;
            this.count = count;
        }
        
        public void run() {
            for (int i = 0; i < count; i++) {
                System.out.println(name + ": " + i);
            }
        }
    }
}
