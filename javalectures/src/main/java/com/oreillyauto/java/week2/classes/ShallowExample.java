package com.oreillyauto.java.week2.classes;

import java.util.Arrays;

public class ShallowExample { 
    private int[] data;
 
    // Makes a shallow copy of values
    public ShallowExample(int[] values) {
        data = values;
    }
 
    public void printData() {
        System.out.println(Arrays.toString(data));
    }
}
