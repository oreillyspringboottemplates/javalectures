package com.oreillyauto.java.week2.classes;

public class MyClass1 {
    final static Double foobar = 3.14D;
    protected static int nonStaticVariable  = 0;
        
    public void foo() {
    	
    }
    
    private void bar(Double foobar) {
    	System.out.println("It works!");
    	foobar = 3.14159D;
    }
    
    public MyClass1() {
        MyInnerClass mic = new MyInnerClass();
        System.out.println(mic.getInnerStringMethod()); 
        System.out.println(MyInnerClass.getInnerMethod());
    }
   
    private static class MyInnerClass {
        protected static final int getInnerMethod() {
            int increment = nonStaticVariable += 1;
            return increment;    
        }
        protected final String getInnerStringMethod() {
            return "Hello World!";
        }
    }
    
    public static void main(String[] args) {
        MyClass1 myone = new MyClass1();
        myone.bar(foobar);
        System.out.println(foobar);
    }
}