package com.oreillyauto.java.week2.day03;

public class Vehicle {
    String type;
    String name;

    public Vehicle(String type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Vehicle [type=" + type + ", name=" + name + "]";
    }

}
