package com.oreillyauto.java.week2.day03;

public class Foo {
	private static String foobar;
	class Class2 {
		// The method doIt cannot be declared static; 
		// static methods can only be declared in a static or top level type
/*		static void doIt() {
			
		}*/
		
		// The member type Class3 cannot be declared static; 
		// static types can only be declared in static or top level types
/*		static class Class3 { 
			
		}*/
	}
}
