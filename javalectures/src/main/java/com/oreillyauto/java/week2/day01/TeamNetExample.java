package com.oreillyauto.java.week2.day01;

public class TeamNetExample {
	
	// Example 5
	public static class ActiveEmployee {
		
		public static int employeeCount = 0;
		
	    public final void getStatus() {
	        System.out.println("Active");
	    }
	    
	    public int getEmployeeCount() {
	    	return employeeCount;
	    }
	    
	    public static String getClassName() {
	    	return ActiveEmployee.class.getSimpleName();
	    }
	}
	
	// Example 6
	public final class Teamnet {
	    protected String CLAZZ = Teamnet.class.getSimpleName();
	       
	    public Teamnet() {
	    }
	    
	    class Credentials {
	        public Credentials(String user, String pass) {
	            
	        }
	    }
	    
	}
	
	
	// Example 7
	// Cannot extend a *final* class
	// The type TeamnetCTT cannot subclass the final class Teamnet
	public class TeamnetCTT /*extends Teamnet*/ { 
	    public TeamnetCTT() {
	    }
	}

}
