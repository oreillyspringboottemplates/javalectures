package com.oreillyauto.java.week2.classes;

public class ThreadRunner {
    public ThreadRunner() {
        for (int i = 0; i < 8; i++) {
            ThreadThatExtends object = new ThreadThatExtends();
            object.start();
        }
    }

    public static void main(String ...args) {
        new ThreadRunner();
    }
}
