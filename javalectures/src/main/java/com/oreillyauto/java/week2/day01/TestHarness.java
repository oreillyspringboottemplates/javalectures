package com.oreillyauto.java.week2.day01;

import java.util.ArrayList;

public class TestHarness {
    //final static variable declared and set
//    final static double PI = 3.141592;
    
    //final static variable declared and not set
//    final static double PI;
    
    //final variable declared and not set
    //final double PI_FINAL;
	//final String foo;
    
    //final static variable declared and set to null
//  final static double PI = null;
    
    //final static ArrayList
    //can we manipulate it?
    final static ArrayList<Integer> MY_ARRAY_LIST = new ArrayList<Integer>();
    
    //constructor 
    TestHarness(){
//        PI=3.142;
        
//        PI_FINAL = 3.142;
//        PI_FINAL = 3.142;
        
//        finalLists();
//        grabStaticMember();
//        incrementStaticVariable();
//        useStaticMethod();
    }

/*    private void useStaticMethod() {
        System.out.println(StaticParent.staticMethodForUse());
        
    }

    @SuppressWarnings("static-access")
    private void incrementStaticVariable() {
        StaticParent a = new StaticParent();
        a.incrementing++;
        StaticParent b = new StaticParent();
            
        if (b.incrementing == 1) {
            System.out.println("Incrementing is shared between instances.");
        } else {
            System.out.println("Incrementing is not shared between instances.");
        }
    }

    private void grabStaticMember() {
        System.out.println(StaticParent.CLAZZ);
    }

    private void finalLists() {
        
        MY_ARRAY_LIST.add(1);
        MY_ARRAY_LIST.add(2);
        System.out.println(MY_ARRAY_LIST);
    }*/

    public static void main(String[] args) {
        
        new TestHarness();

    }
}