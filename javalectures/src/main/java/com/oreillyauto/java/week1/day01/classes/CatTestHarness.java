package com.oreillyauto.java.week1.day01.classes;

public class CatTestHarness {

    public CatTestHarness() {
        Kitten kitty = new Kitten("Ginny", "Calico");
        kitty.setFluffyLevel(5);
        System.out.println("kitty: " + kitty);
        System.out.println(kitty.getName() + "'s fluffy level " + kitty.getFluffyLevel());
    }
    
    public static void main(String[] args) {
        new CatTestHarness();
    }

}
