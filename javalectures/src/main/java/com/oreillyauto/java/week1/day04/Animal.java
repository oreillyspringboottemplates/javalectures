package com.oreillyauto.java.week1.day04;

public class Animal {
	public int numberOfTeeth;
	public String eyeColor;
	int debug = 0;
	
	Animal() {
		if (debug > 0) {
			System.out.println("Animal Empty Constructor called!");
		}
	}
	
	public Animal(int numberOfTeeth, String eyeColor) {
		this.numberOfTeeth = numberOfTeeth;
		this.eyeColor = eyeColor;
		
		if (debug > 0) {
			System.out.println("Animal Constructor(int, String) called!");
		}
	}

	public String animalTime() {
		return "It's animal time!";
	}

	// getters and setters
	public int getNumberOfTeeth() {
		return numberOfTeeth;
	}

	public void setNumberOfTeeth(int numberOfTeeth) {
		this.numberOfTeeth = numberOfTeeth;
	}

	public String getEyeColor() {
		return eyeColor;
	}

	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}
}
