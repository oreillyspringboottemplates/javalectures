package com.oreillyauto.java.week1.day03.exceptions;
//import org.apache.log4j.spi.ErrorCode;

/**
 * 
 * KNOW HOW TO BUILD A CUSTOM EXCEPTION CLASS
 * THIS WILL BE ON THE MILESTONE!!
 * 
 */

public class UnderageException extends Exception {

    //private ErrorCode code;
    private static final long serialVersionUID = 893577274883254367L;

    // Pass message and generated "cause" to the super class Exception
    public UnderageException(String message) {
        super(message, new Throwable(message));
    }
    
    // Pass message and cause to the super class Exception
    public UnderageException(String message, Throwable cause) {
        super(message, cause);
    }
    
    // Pass message and cause to the super class Exception
    // Also handle a custom code
    // Codes are used a lot with RESTful Web Services
//    public UnderageError(String message, Throwable cause, ErrorCode code) {
//        super(message, cause);
//        //this.code = code;
//    }
    
//    public ErrorCode getErrorCode() {
//        return code;
//    }
}