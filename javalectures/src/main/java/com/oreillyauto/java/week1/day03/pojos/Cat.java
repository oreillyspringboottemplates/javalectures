package com.oreillyauto.java.week1.day03.pojos;

public class Cat extends Animal {
     
    public Cat() {}

    public Cat(String type) {
        super(type);
    }

    public Cat(String type, String color) {
        super(type, color);
    }
    
    @Override
    public String sound() {
    	return "prrrr";
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object that) {
        
    	if (this == that)
            return true;
        
        if (!super.equals(that))
            return false;
        
        if (getClass() != that.getClass())
            return false;
        
        Cat other = (Cat) that;
        
        if (color == null) {
            if (other.color != null)
                return false;
        } else if (!color.equals(other.color)) {
            return false;
        }
        
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type)) {
        	return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Cat [type=" + type + ", color=" + color + "]";
    }

}
