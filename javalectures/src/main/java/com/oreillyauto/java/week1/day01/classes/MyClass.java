package com.oreillyauto.java.week1.day01.classes;

public class MyClass {
    public String accessible;
    private String encapsulated;
    private int count = 0;  
    
    private void privateMethod() {
    }
    
    public void publicMethod() {
    	this.privateMethod();
    }
    
    
    
}
