package com.oreillyauto.java.week1.day04;

class Reptile extends Animal {
	
	@Override
	public String animalTime() {
		return "It's reptile time!";
	}

	public String invokeSuperTime() {
		return super.animalTime();
	}
}