package com.oreillyauto.java.week1.day01.pojos;

import java.math.BigDecimal;

public class IceCream {

	private BigDecimal temperature;
	private String flavor;

	public IceCream() {
		
	}

	public BigDecimal getTemperature() {
		return temperature;
	}

	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}

	public String getFlavor() {
		return flavor;
	}

	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}
		

	@Override
	public String toString() {
		return "IceCream [temperature=" + temperature + ", flavor=" + flavor + "]";
	}

	public static void main(String[] args) {
		IceCream myIceCream = new IceCream();
		myIceCream.setFlavor("chocolate");
		myIceCream.setTemperature(new BigDecimal("15.3"));
		System.out.println(myIceCream);
	}

}
