package com.oreillyauto.java.week1.day04.example2.Books;

import com.oreillyauto.java.week1.day04.example2.Media;
import com.oreillyauto.java.week1.day04.example2.Interfaces.Physical;

public class Book extends Media implements Physical {

	private int pages = 0;
	
	
	public Book(String author, int pages) {
		setAuthor(author);
		this.setPages(pages);
	}
	
	public Book() {
		
	}
	
	public String getAuthor() {
		return this.getCreator();
	}
	
	public void setAuthor(String author) {
		this.setCreator(author);
	}

	@Override
	public String throwIt() {
		return "YeeEt";
	}

	@Override
	public String smellIt() {
		return "Smells old";
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

}
