package com.oreillyauto.java.week1.day04;

class Mammal extends Animal {
	int debug = 0;
	
	
	public Mammal() {
		if (debug > 0) {
			System.out.println("Mammal constructor called!");
		}
	}

	public Mammal(String eyeColor) {
		super(0, eyeColor);
		
		if (debug > 0) {
			System.out.println("Mammal constructor called");
		}
	}

	@Override
	public String animalTime() {
		return "It's mammal time!";
	}

	// Call the super class animalTime method
	public String invokeSuperTime() {
		return super.animalTime();
	}
	
	
}
