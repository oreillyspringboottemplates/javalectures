package com.oreillyauto.java.week1.day01.pojos;

public class Cat extends Animal {
    
    public Cat() {
		super();
	}

	public static void main(String[] args) {	    
    	Cat myCat = new Cat();
    	Animal myAnimal = (Animal)myCat; // type casting
    	myAnimal.hide();   // Animal? Cat?
    	myAnimal.override(); // Animal? Cat?
    	myAnimal.setType("Mainecoon");
    	
    	System.out.println(myCat.getType());
    	
    	
    	System.out.println(myAnimal.sound());
    	
    	Animal animal = new Animal();
    	animal.hide();
    	animal.override();
    }
    
	private void scream() {
		System.out.println("MEOOOOOOWWWWW!!!!!!!!!!!!!!");
	}
	
	@Override
    public void hide() {
    	System.out.println("The hide method in Cat was called!");
    }
    
    @Override
    public void override() {
    	System.out.println("The override method in Cat was called!");
    }
    
    public Cat(String type) {
        super(type);
    }

    public Cat(String type, String color) {
        super(type, color);
    }
    
    @Override
    public String sound() {
    	return "prrrr";
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cat other = (Cat) obj;
        if (color == null) {
            if (other.color != null)
                return false;
        } else if (!color.equals(other.color))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Cat [type=" + type + ", color=" + color + "]";
    }

}
