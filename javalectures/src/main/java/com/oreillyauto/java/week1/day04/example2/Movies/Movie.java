package com.oreillyauto.java.week1.day04.example2.Movies;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.oreillyauto.java.week1.day04.example2.Media;
import com.oreillyauto.java.week1.day04.example2.Interfaces.Digital;

public class Movie extends Media implements Digital {

	private String director = "";
	private int runTimeInMinutes = 0;
	private List<String> mainActors = new ArrayList<String>();
	private BigDecimal boxOffice = BigDecimal.ZERO;

	public Movie(String director, int runTimeInMinutes, List<String> mainActors, BigDecimal boxOffice) {
		this.director = director;
		this.runTimeInMinutes = runTimeInMinutes;
		this.mainActors = mainActors;
		this.boxOffice = boxOffice;
	}

	public Movie() {
		// TODO Auto-generated constructor stub
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getRunTimeInMinutes() {
		return runTimeInMinutes;
	}

	public void setRunTimeInMinutes(int runTimeInMinutes) {
		this.runTimeInMinutes = runTimeInMinutes;
	}

	public List<String> getMainActors() {
		return mainActors;
	}

	public void setMainActors(List<String> mainActors) {
		this.mainActors = mainActors;
	}

	public BigDecimal getBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(BigDecimal boxOffice) {
		this.boxOffice = boxOffice;
	}
	
	public String toString() {
		return new Gson().toJson(this);
	}

	@Override
	public String watchIt() {
		return "watching the movie!";
	}

	@Override
	public String downloadFromShadyWebsite() {
		return "I paid for this!";
	}

	@Override
	public boolean hasBeenWatchedByIsp() {
		return true;
	}

}
