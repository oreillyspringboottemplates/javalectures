package com.oreillyauto.java.week1.day03;

import java.util.ArrayList;
import java.util.List;

public class TestObjects {
	public TestObjects() {
		List<Integer> integerList = new ArrayList<Integer>();
		integerList.add(1);
		//printObjectList(integerList);
		
		
		try {
			String s = null;
			System.out.println(s.toCharArray());
		} catch (NullPointerException npe) {
			System.out.println("npe instanceof null: " + (npe.getMessage() instanceof String));
			System.out.println(npe.getMessage());
		}
	}
	
	private void printObjectList(List<Object> objectList) {
		for (Object object : objectList) {
			System.out.println(object);
		}
	}
	
	public static void main(String[] args) {
		new TestObjects();
	}
}
