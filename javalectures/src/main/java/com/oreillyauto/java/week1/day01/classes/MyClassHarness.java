package com.oreillyauto.java.week1.day01.classes;

public class MyClassHarness {
	public MyClassHarness() {
	}

	public static void main(String[] args) {
		MyClass myClass = new MyClass();
		String accessed = myClass.accessible;
		
		// This won't compile since the member variable is private (encapsulated)
//		String notAccessed = myClass.encapsulated;
		
		myClass.publicMethod();
	}
}
