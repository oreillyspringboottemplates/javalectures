package com.oreillyauto.java.week1.day01;

public class OverloadingTestHarness {

	public OverloadingTestHarness() {
		System.out.println(count(new Integer[] {1,2,3}, "foo"));
	}
	
	public int count(Integer[] array) {
	    int sum = 0;
	   
	    for (Integer i : array) {
	        sum = sum + i;
	    }
	    return sum;
	}
	 
	public int count(Integer[] integers, String foo) {
		System.out.println(foo);
	    int sum = 0;   

	    for (Integer i : integers) {
	        sum = sum + i;
	    }
	    return sum;
	}
	
	/*public int count(Integer... integers, String foo) {
	    int sum = 0;   

	    for (Integer i : integers) {
	        sum = sum + i;
	    }
	    return sum;
	}*/


	public static void main(String[] args) {
		new OverloadingTestHarness();
	}
}
