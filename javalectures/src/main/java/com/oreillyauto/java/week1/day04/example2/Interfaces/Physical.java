package com.oreillyauto.java.week1.day04.example2.Interfaces;

public interface Physical {
	String throwIt();
	String smellIt();
}
