package com.oreillyauto.java.week1.day04;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.oreillyauto.java.classes.AgeComparator;
import com.oreillyauto.java.classes.NameComparator;
import com.oreillyauto.java.classes.Ocean;
import com.oreillyauto.java.classes.Player;
import com.oreillyauto.java.classes.Student;
import com.oreillyauto.java.week1.day03.pojos.Cat;
import com.oreillyauto.java.week1.day04.classes.Alternator;
import com.oreillyauto.java.week1.day04.interfaces.OrderImpl;

public class W5D4TestHarness {
	
	
	public W5D4TestHarness() {
//		testAnimalTime();  		// Example 1
//		testConstructors(); 	// Example 2 - set debug to 1 in Animal and Mammal
//		testMammalForAnimal(); 	// Example 3 - set debug to 0 in Animal and Mammal
//		testSuperclassLists(); 	// Example 4 -  
//		extendingExample(); 	// Example 5 - 
//		testInterfaces(); 		// Example 6 - 
//		abstractClass(); 		// Example 7 - 
//		makeItRain(); 			// Example 8 - 
//		testLinkedList(); 		// Example 9 - 
		testComparable(); 		// Example 10 - 
		testComparator(); 		// Example 11 - 
		
		tests();
	}

	private void tests() {
		List<Integer> myList = new ArrayList<>();
		
		List<Object> list = new ArrayList<Object>();
		Animal a = new Animal();
		list.add(a);
		list.add(1);
		list.add(true);
		list.add(new Cat());
		printObject(list);
		
		List<Animal> animalList = new ArrayList<Animal>();
//		printObject(animalList);  // DOES NOT COMPILE
	}

	private void printObject(List<Object> objList) {
				
	}

	private void testSuperclassLists() {
		Reptile reptile = new Reptile();
		Mammal mammal = new Mammal();
		Animal animal = new Animal();
		Bird bird = new Bird();

		List<Animal> animalList = new ArrayList<Animal>();
		animalList.add(reptile);
		animalList.add(mammal);
		animalList.add(bird);
		animalList.add(animal);

		for (Animal currentAnimal : animalList) {
			System.out.println(currentAnimal.animalTime());
		}
	}

	private void testMammalForAnimal() {
		Mammal mammal = new Mammal();
		animalAsArgument(mammal);
	}

	private void animalAsArgument(Animal animal) {
		System.out.println(animal.animalTime());

        if (animal instanceof Animal) {
            System.out.println("Animal passed in is instance of Animal.");
        }
        
        if (animal instanceof Mammal) {
            System.out.println("Animal passed in is instance of Mammal.");
        }
	}

	private void testConstructors() {
		Mammal mammal = new Mammal("blue");
		System.out.println("mammal.getNumberOfTeeth():\t" + mammal.getNumberOfTeeth());
		System.out.println("mammal.getEyeColor():\t\t" + mammal.getEyeColor());
		System.out.println();
	}
	
    private void testAnimalTimeOLD() {
        //Mammal mammal = new Mammal();
        //System.out.println("mammal.animalTime():\t\t" + mammal.animalTime());
        //System.out.println("mammal.invokeSuperTime():\t" + mammal.invokeSuperTime());
    }
    
	private void testAnimalTime() {
		Mammal mammal = new Mammal();
        System.out.println("mammal.animalTime():\t\t" + mammal.animalTime());
        System.out.println("mammal.invokeSuperTime():\t" + mammal.invokeSuperTime());
        System.out.println();

	    System.out.println("mammal instance of Animal => " + (mammal instanceof Animal) );    
	    System.out.println("mammal instance of Mammal => " + (mammal instanceof Mammal) );
	    System.out.println("mammal instance of Object => " + (mammal instanceof Object) );
	    
	    Animal mammal2 = new Mammal();
	    System.out.println("mammal instance of Animal => " + (mammal2 instanceof Animal) );    
	    System.out.println("mammal instance of Mammal => " + (mammal2 instanceof Mammal) );
	    System.out.println("mammal instance of Object => " + (mammal2 instanceof Object) );  
	    System.out.println();
	}

	private void abstractClass() {
		Alternator hondaAlt = new Alternator("alt01", "A fine alt for Honda Civics 2011-2015");
		hondaAlt.setMSRP(new BigDecimal("100.01"));
		System.out.println(hondaAlt);
	}
	
	private void testComparator() {
		List<Student> al = new ArrayList<Student>();
		al.add(new Student(101, "Vijay", 23));
		al.add(new Student(106, "Jai", 27));
		al.add(new Student(105, "Ajay", 27));

//		System.out.println("Sorting by Name...");
//
//		Collections.sort(al, new NameComparator());
		
//		Collections.sort(al, new Comparator<Student>() {
//            @Override
//            public int compare(Student o1, Student o2) {
//                // TODO Auto-generated method stub
//                return 0;
//            }
//		});
		
		
//		Iterator<Student> itr = al.iterator();
//
//		while (itr.hasNext()) {
//			Student st = (Student) itr.next();
//			System.out.println(st.rollno + " " + st.name + " " + st.age);
//		}

//		System.out.println("sorting by age...");
//
//		Collections.sort(al, new AgeComparator());
//		Iterator<Student> itr2 = al.iterator();
//
//		while (itr2.hasNext()) {
//			Student st = (Student) itr2.next();
//			System.out.println(st.rollno + " " + st.name + " " + st.age);
//		}

	}

	private void testComparable() {
		Player player1 = new Player("Bob", 2);
		Player player2 = new Player("Sue", 1);
		LinkedList<Player> playerList = new LinkedList<Player>();
		playerList.add(player1);
		playerList.add(player2);
		Collections.sort(playerList);
		
//		Collections.sort(Arrays.asList(new Integer[]{1,2,3}));

		/** Example 1 */
		// Edit the compare function in Player
		// Ensure sort by rank is uncommented
		
		/** Example 2 */
		// Edit the compare function in Player
		// Ensure sort by name is uncommented
		
		for (Player player : playerList) {
			System.out.println(player);
		}
	}

	private void testLinkedList() {
		// Creating linkedList of class linked list
		LinkedList<String> linkedList = new LinkedList<String>();
		             
		// letters : D A E B C F B G 
		// position: 0 1 2 3 4 5 6 7 ?		
		// Adding elements to the linked list
//		linkedList.add("A");      // queue
//		linkedList.add("B");      // queue
//		linkedList.addLast("C");  // add last!
//		linkedList.addFirst("D"); // add first! - add at index 0
//		linkedList.add(2, "E");   // insert into position [2]
//		linkedList.add("F");      // queue
//		linkedList.add("B");      // queue
//		linkedList.add("G");      // queue 
//		System.out.println("Linked list : " + linkedList); // output? DAEBCFBG

		// letters : A E F B
		// position: 0 1 2 3 4 5 6 7
		// Removing elements from the linked list
//		linkedList.remove("B"); // first occurrence
//		linkedList.remove(3);   //  
//		linkedList.removeFirst();
//		linkedList.removeLast();
//		System.out.println("Linked list after deletion: " + linkedList); // ?

		// Offer vs Add
		// offer returns false if it can't add at the end
		// add throws an exception if it can't add
		
		// Finding elements in the linked list
//		boolean status = linkedList.contains("E");
//
//		if (status)
//			System.out.println("List contains the element \"E\" ");
//		else
//			System.out.println("List doesn't contain the element \"E\"");

		// Number of elements in the linked list
//		int size = linkedList.size();
//		System.out.println("Size of linked list = " + size);

		// A E F B
		// Get and set elements from linked list
//		Object element = linkedList.get(2);
//		System.out.println("Element returned by get() : " + element);
//		linkedList.set(2, "Y");
//		System.out.println("Linked list after change : " + linkedList);
	}

	private void makeItRain() {
		// Classes in this example: 
		//   MyAbstractWater.java (Abstract)
		//   Ocean.java (Concrete)
		//   Type.java (Enum) {"salty", "muddy", "instant"}
		
		// We cannot instantiate abstract classes.
//		MyAbstractWater maw = new MyAbstractWater();
		// Compilation error: MyAbstractWater cannot be resolved to a type
		
		// Ocean extends MyAbstractWater
		//   implements abstract method getType()
		// Gets the type of water found in the ocean...
//		Ocean deadSea = new Ocean();
//		System.out.println("type: " + deadSea.getType());
	}

	private void testInterfaces() {
		OrderImpl oi = new OrderImpl();
		oi.showNumberOfItems(3);
		oi.showOrderType();
		oi.showDate(); 				// default method
		oi.showStoreNumber(2345);
		oi.showSpecialOrderId(322);
		
		oi.showDate(); 				// uncomment out RetailOrder's override
		oi.showDate("Pat"); 		// overloading
	}

	private void extendingExample() {
		Dolphin d = new Dolphin();
		System.out.println("d.animalTime():\t\t" + d.animalTime());
		System.out.println("d.invokeSuperTime():\t" + d.invokeSuperTime());
	}

//    private void testConstructors() {
//        Mammal mammal = new Mammal("intern");
//        System.out.println("mammal.getI():\t" + mammal.getI());
//        System.out.println("mammal.getJ():\t" + mammal.getJ());
//    }

//    private void testMammalForAnimal() {
//        Mammal mammal = new Mammal();
//        animalAsArgument(mammal);
//    }
//    private void animalAsArgument(Animal animal) {
//        System.out.println(animal.animalTime());
//    }

//    private void animalAsArgument(Animal animal) {
//
//        if (animal instanceof Animal) {
//            System.out.println("Animal passed in is instance of Animal.");
//        }
//
//        if (animal instanceof Mammal) {
//            System.out.println("Animal passed in is instance of Mammal.");
//        }
//    }

	public static void main(String[] args) {
		new W5D4TestHarness();
	}
}
