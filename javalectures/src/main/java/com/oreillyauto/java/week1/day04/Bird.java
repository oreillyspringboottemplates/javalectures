package com.oreillyauto.java.week1.day04;

public class Bird extends Animal {

	public Bird() {
		System.out.println(this.getClass().getName());
	}

	public Bird(int numberOfTeeth, String eyeColor) {
		super(numberOfTeeth, eyeColor);
	}
	
	public static void main(String[] args) {
		new Bird();
	}

}
