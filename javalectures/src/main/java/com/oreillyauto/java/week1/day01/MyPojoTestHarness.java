package com.oreillyauto.java.week1.day01;

import com.oreillyauto.java.week1.day01.pojos.MyPojo;

public class MyPojoTestHarness {

	public MyPojoTestHarness() {
		System.out.println(MyPojo.accessible);
		MyPojo mp = new MyPojo();     				// Instantiate MyPojo
		System.out.println(mp.getEncapsulated());   // Call public method
		mp.incrementCount();
		System.out.println(mp.getCount());
		
		if ("".equals(mp.getEncapsulated()) || mp.getEncapsulated() == null) {
			System.out.println("Dude, seriously!");
			
			if (mp.getEncapsulated() == null) {
			    System.out.println("It's null!");    
			}
		}
		
		mp.setEncapsulated("uhh yeah");
		System.out.println(mp.getEncapsulated());
	}

	public static void main(String[] args) {
	    new MyPojoTestHarness();
	}

}
