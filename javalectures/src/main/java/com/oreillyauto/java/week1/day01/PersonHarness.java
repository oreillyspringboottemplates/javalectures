package com.oreillyauto.java.week1.day01;

public class PersonHarness {

    public static void main(String[] args) {
        Person frank = new Person("Frank", 3453);
        //Person frank = new Person();
        frank.printPerson();
        
        Person whateveryouwanttonamethem = new Person();
        whateveryouwanttonamethem.setId(123456);
        whateveryouwanttonamethem.setName("W");
        
        System.out.println(whateveryouwanttonamethem.getName());
    }

}
