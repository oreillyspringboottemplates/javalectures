package com.oreillyauto.java.week1.day01;

import com.oreillyauto.java.helpers.DateTimeHelper;
import com.oreillyauto.java.week1.day01.classes.DefaultClockTimex;

public class W5D1TestHarness {

    public W5D1TestHarness() {
    	/* PUBLIC ACCESSOR EXAMPLE */
//    	runPublicExample();
    	
    	/* PROTECTED ACCESSOR EXAMPLE */
//    	runProtectedExample();
    	       
        /* DEFAULT ACCESSOR EXAMPLE */
//    	runDefaultExample();
        
        /* PRIVATE ACCESSOR EXAMPLE */
        runPrivateExample();
    }
    
    private void runPrivateExample() {
		/**
		   - View PrivateClock.java
		   - Notice that time has a private accessor
		   - This means that only the owning class has access to this
		     variable, or the variable can be exposed with a getter
		     and/or a setter.
		   -   
		 */
		PrivateClock privateClock = new PrivateClock();
//		System.out.println(privateClock.time);
		
		// ^== ERROR: The field PrivateClock.time is not visible
	}

	private void runDefaultExample() {
        /** 
           Test the Default Clock Timex Class
           - DefaultClockTimex.java is in the /day1/classes package
           - DefaultClock.java is in the /day1 package
           - DefaultClockTimex extends DefaultClock
           - DefaultClockTimex is attempting to access a member variable
             in another class that is in another package.
           - The default accessor method, however, does not allow access 
             to member variables outside its own package.
           - Therefore accessing timex.time is not allowed and the 
             code will not compile.  
         */
        DefaultClockTimex timex = new DefaultClockTimex();
        DefaultClock dc = new DefaultClock();
        System.out.println(dc.time);
//        System.out.println("timex.time:" + timex.time);
        
        // ^== ERROR: The field DefaultClock.time is not visible
	}

	private void runProtectedExample() {
    	/* PROTECTED */
		/** 
		 * TAKE 1 - Protected Access Denied
		   - View SmartProtectedClock.java 
		   - View /classes/WorldClass.java
		   - Notice that WorldClass.java is not
		      -- in the same class
		      -- in the same package
		      -- a subclass
		   - So, WorldClass.java is a "World Class" that is 
		     attempting to access a protected member variable.    
		   - As a result, the code does not compile.  
    	*/
    	
    	/**
    	 * TAKE 2 - 
    	   - View SmartClock.java 
    	   - If we extend another class (subclass), then we would
    	     have access to the protected super class members.
    	   - SmartClock.getTimeInSeconds() returns "this.time / 1000;"
    	   - While the method is public, "time" is protected.
    	   - This method will execute successfully since protected
    	     member variables can be accessed by a subclass.
    	   - When "this" is printed, does it refer to SmartClock or
    	     SmartProtectedClock?
    	 */
		SmartClock mySmartClock = new SmartClock();
		System.out.println(mySmartClock.time);
		System.out.println(mySmartClock.getTimeInSeconds());
	}

	private void runPublicExample() {
    	/* PUBLIC ACCESSOR EXAMPLE */
        // Test the clock with a public access modifier
    	// For the fun of it, we will convert the Time for this 
    	// example. Going forward we will simply return a "Long."
        ClockReader clockReader = new ClockReader();
        Long time = clockReader.readClock();
        String timeStr = DateTimeHelper.getTime(time, "HH:mm:ss.SSS"); 
        System.out.println("time: " + time + " timeStr: " + timeStr);
    }

    public static void main(String[] args) {
        new W5D1TestHarness();
    }

}
