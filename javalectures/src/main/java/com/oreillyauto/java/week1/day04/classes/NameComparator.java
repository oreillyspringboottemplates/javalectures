package com.oreillyauto.java.week1.day04.classes;

import java.util.Comparator;

public class NameComparator implements Comparator<Student> {
    public int compare(Student s1, Student s2) {
        if (s1.age == s2.age) {
        	return s1.name.compareTo(s2.name);
        } else if (s1.age > s2.age) {
            return 1;
        } else {
            return -1;
        }
    }
}
