package com.oreillyauto.java.week1.day01;

import java.math.BigDecimal;

public class BigDecimalTestHarness {
	public BigDecimalTestHarness() {
		BigDecimal one = new BigDecimal("1");
		BigDecimal two = new BigDecimal("2");
		System.out.println(one.add(two));  // 3?
		
		System.out.println(one);            // 1 ?
		System.out.println(sum(one, two));  // 3?
	}
	

	private BigDecimal sum(BigDecimal one, BigDecimal two) {
		BigDecimal total = new BigDecimal("0");
		total = total.add(one);
		total = total.add(two);
		return total;						
	}

	public static void main(String[] args) {
		new BigDecimalTestHarness();
	}
}
