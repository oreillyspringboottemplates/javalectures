package com.oreillyauto.java.week1.day04.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.oreillyauto.java.week1.day04.example.air.Helicopter;
import com.oreillyauto.java.week1.day04.example.land.Bicycle;
import com.oreillyauto.java.week1.day04.example.sea.JetSki;

public class VehicleTestHarness {

    public VehicleTestHarness() {
        List<BaseVehicle> vehicleList = new ArrayList<BaseVehicle>();

        synchronized(this) {
            JetSki jski = new JetSki();
            vehicleList.add(jski);
            
            Bicycle bike = new Bicycle();
            bike.setWheels(2);
            vehicleList.add(bike);
            
            Helicopter heli = new Helicopter();
            heli.setWheels(0);
            heli.setDoors(2);
            vehicleList.add(heli);
            
            Collections.sort(vehicleList);
            
            for (BaseVehicle baseVehicle : vehicleList) {
                System.out.println(baseVehicle);
            }
        }
    }
    
    public static void main(String[] args) {
        new VehicleTestHarness();
    }

}
