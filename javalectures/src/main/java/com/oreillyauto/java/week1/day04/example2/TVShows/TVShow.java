package com.oreillyauto.java.week1.day04.example2.TVShows;

import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.java.week1.day04.example2.Media;
import com.oreillyauto.java.week1.day04.example2.Interfaces.Digital;

public class TVShow extends Media implements Digital {

	private int numSeasons = 0;
	private int[] episodes = new int[0];
	private List<String> mainActors = new ArrayList<String>();
	
	public TVShow(int numSeasons, int[] episodes) {
		super();
		this.setNumSeasons(numSeasons);
		this.episodes = episodes;
	}
	
	public TVShow(int numSeasons) {
		super();
		this.setNumSeasons(numSeasons);
		this.episodes = new int[numSeasons];
	}
	
	public TVShow() {
		super();
	}
	
	public void setEpisodesInSeason(int season, int episodesNum) {
		episodes[season] = episodesNum;
	}

	public int getNumSeasons() {
		return numSeasons;
	}

	public void setNumSeasons(int numSeasons) {
		this.numSeasons = numSeasons;
	}

	public List<String> getMainActors() {
		return mainActors;
	}

	public void setMainActors(List<String> mainActors) {
		this.mainActors = mainActors;
	}

	@Override
	public String watchIt() {
		return "jolly good show! Watched all "+getNumSeasons()+" of it.";
	}

	@Override
	public String downloadFromShadyWebsite() {
		return "OOOweee";
	}

	@Override
	public boolean hasBeenWatchedByIsp() {
		return false;
	}

}
