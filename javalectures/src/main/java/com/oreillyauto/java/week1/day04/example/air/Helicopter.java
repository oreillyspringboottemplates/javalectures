package com.oreillyauto.java.week1.day04.example.air;

import com.oreillyauto.java.week1.day04.example.interfaces.MotorTransport;

public class Helicopter extends AirVehicle implements MotorTransport {

    public Helicopter() {
        System.out.println("Dude you got a Heli! Heli Cool!");
    }

    @Override
    public String vroom() {
        return "ROFL";
    }

    @Override
    public Integer gallonsOf() {
        return 2;
    }

    @Override
    public String hornSound() {
        return "i've fallen and I can't get up";
    }

}
