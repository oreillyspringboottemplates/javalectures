package com.oreillyauto.java.week1.day01.pojos;

import java.math.BigDecimal;

public class MyPojo {
	
	public final BigDecimal PI;// = new BigDecimal("3.14"); 
	public static String accessible = "Pfhhhhh";
	private int count = 0;
	private String encapsulated = "";
	
	public MyPojo() {
		PI = new BigDecimal("3.14");
	}

	public void incrementCount() {
		count += 1;	
	}
	
	public static String getAccessible() {
		return accessible;
	}

	public static void setAccessible(String accessible) {
		MyPojo.accessible = accessible;
	}

	public String getEncapsulated() {
		return encapsulated;
	}

	public void setEncapsulated(String inEncapsulated) {
		this.encapsulated = inEncapsulated;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public BigDecimal getPI() {
		return PI;
	}

	
	
}
