package com.oreillyauto.java.week1.day04.classes;

import java.math.BigDecimal;

public abstract class AbstractProduct {
	private String sku = "sku not set";
	private String description = "description not set";
	private BigDecimal MSRP = BigDecimal.ZERO; // new BigDecimal("0");
	
	AbstractProduct(String sku, String description){
		this.sku = sku;
		this.description = description;
	}
		
	abstract BigDecimal getSalesPrice();

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getMSRP() {
		return MSRP;
	}

	public void setMSRP(BigDecimal mSRP) {
		MSRP = mSRP;
	}
}
