package com.oreillyauto.java.week1.day04.interfaces;

public interface MyInterface extends AnotherInterface, YetAnotherInterface {
	
    public void doSomething(String a, String b);
    
    default String doSomethingElse(String x, String y) {
        return "x=" + x + " and y = " + y;
    }
    
    public static void printMessage(String message) {
    	System.out.println("Message: " + message);
    }
}
