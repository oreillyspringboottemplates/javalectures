package com.oreillyauto.java.week1.day04.example;

import com.google.gson.Gson;

public abstract class BaseVehicle implements Comparable<BaseVehicle> {

	public static final Integer LUDICROUS_SPEED = Integer.MAX_VALUE;
	
    protected int wheels = 0;
    protected int doors = 0;
    protected int maxSpeed = LUDICROUS_SPEED;
    protected int currentSpeed = 0;
    protected int acceleration = 0;

    
    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }

    @Override
    public String toString() {
    	return new Gson().toJson(this);    	
//        StringBuilder sb = new StringBuilder();
//        sb.append("{");
//        sb.append("\"Name\" : \"" + this.getClass().getSimpleName() + "\", ");
//        sb.append("\"Doors\" : \"" + this.getDoors() + "\", ");
//        sb.append("\"Wheels\" : \"" + this.getWheels() + "\" ");
//        sb.append("}");
//        return sb.toString();
    }

    public int compareTo(BaseVehicle baseVehicle) {
        return this.getClass().getSimpleName().compareTo(baseVehicle.getClass().getSimpleName());
    }

}
