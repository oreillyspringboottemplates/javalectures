package com.oreillyauto.java.week1.day03;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.oreillyauto.java.helpers.Helper;
import com.oreillyauto.java.week1.day03.classes.MyGenericClass;
import com.oreillyauto.java.week1.day03.classes.MyGenericClass3;
import com.oreillyauto.java.week1.day03.enums.Month;
import com.oreillyauto.java.week1.day03.exceptions.UnderageException;
import com.oreillyauto.java.week1.day03.pojos.Animal;
import com.oreillyauto.java.week1.day03.pojos.Cat;
import com.oreillyauto.java.week1.day03.pojos.Pair;


public class W5D3TestHarness {

    public W5D3TestHarness() {
//        testEnums();
//        testGenericClass();
//        testGenericClass2();
//        testGenericClass3();
//        testGenericClass4();
//        testWildcard();
//        testCheckedExceptions();
//        testCustomExceptions();
//        testRuntimeExceptions();
//        testScanner();        
//        testFileReader(); // This example uses the local file system (bad for web dev!!!!!!!!!!!!!!!!)
//    	  testDefaultClassPath(); // This example references the file in the root of the project (good)
//    	  testPackageReference(); // This example references the file in a package (good)
//        quickTests();
    }  

    private void testPackageReference() {
    	// This example points to a file in a package
    	// Notice that there's no forward slash before src
    	
    	try (BufferedReader br = new BufferedReader(new FileReader(new File("src/com/oreillyauto/java/files/file1.txt")))) {    	
    		String st;
    		List<String> values = new ArrayList<String>();
    		
    		while ((st = br.readLine()) != null) {
    			values.addAll(Arrays.asList(st.replaceAll("^\"", "").split("\"?(,|$)(?=(([^\"]*\"){2})*[^\"]*$) *\"?")));
    		}
    		
    		int sum = 0;
    		StringBuilder sb = new StringBuilder();
    		
    		for (String s: values) {
    			sb.append((sum == 0) ? s : (" " + s));
    			sum += Integer.parseInt(s);
    		}
    		
    		System.out.println("File Contents: " + sb.toString());
    		System.out.println("The sum is " + sum);
    		
    		
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
	}

	private void testDefaultClassPath() {
		try {
			// This works because file1.txt is saved to the root of this project
			// The root of this project is the "default classpath"
			try (BufferedReader br2 = new BufferedReader(new FileReader(new File("file1.txt")))) {
				String st;

				while ((st = br2.readLine()) != null) {
					System.out.println(st);
				}
			}
		} catch (Exception e) {
			/* do nothing */
			e.printStackTrace();
		}        
	}

	private void testEnums() {
    	    	
    	// Review Color.java (this is the com.oreillyauto.java object)
//    	System.out.println(Color.RED.ordinal());
//    	System.out.println(Color.RED);
//    	System.out.println(Color.RED.name());
    	
    	
    	// Review Month.java
    	
        /** Test Month Enum */
        Month jan = Month.JANUARY;
        Month mar = Month.MARCH;        
//        System.out.println("Month.January => " + jan);
//        System.out.println("Month.January.quarter => " + jan.getQuarter());
//        System.out.println("Month.January.season => " + jan.getSeason());
//        System.out.println("jan.ordinal => " + jan.ordinal());
//        System.out.println("mar.toString() => " + mar.toString());
        
        // You can obtain an array of all the possible values of a Java 
        // enum type by calling its static values() method. 
        // All enum types get a static values() method automatically by 
        // the Java compiler
//        for (Month mon : Month.values()) {
//            System.out.println(mon);
//        }
        
        // Methods to retrieve private properties can be used for 
        // better encapsulation. Quarter and Season are private members
        // of the Enum, but we can use public methods to retrieve the
        // values.
//        System.out.println("Quarter and Season (" + jan.name() + "): " +  
//        		jan.getQuarterAndSeason());
        
        
        /** Test Status Enum */
        // Review Status.java
//        Status waiting = Status.WAITING_CONFIRMATION;
//        System.out.println("Status.WAITING_CONFIRMATION => " + waiting); // 3
//        
//        System.out.println("Waiting Confirmation Literal => " + 
//        		Status.WAITING_CONFIRMATION.getStatusLiteral());  //  WAITING_CONFIRMATION
//        
//        System.out.println("Waiting Confirmation Viewable Literal => " + 
//        		Status.WAITING_CONFIRMATION.getViewableStatusLiteral()); // WAITING CONFIRMATION

    }

	/**
	 * Generics 
	 * Generic Type is a generic class or interface that is parameterized over types.
	 *  
	 * Briefly, generics enable types (classes and interfaces) to be parameters when 
	 * defining classes, interfaces and methods. 
	 * 
	 * Just as we can pass a specific data type as a parameter into a method, type 
	 * parameters provide a way for you to re-use the same code with different inputs. 
	 * The difference is that the inputs to formal parameters are values, while the 
	 * inputs to type parameters are types.
	 * 
	 * Code that uses generics has many benefits over non-generic code:
	 * Stronger type checks at compile time.
	 * 
	 * A Java compiler applies strong type checking to generic code and issues errors if 
	 * the code violates type safety. Fixing compile-time errors is easier than fixing 
	 * runtime errors, which can be difficult to find.
	 * 
	 */
    private void testGenericClass() {
        /** Code that uses generics has many benefits over non-generic code */
    	
    	/**  1. Strong type checking     */
//    	foo = 5;  // can't do this; this is not JavaScript
        
        
    	/**  2. Elimination of types is possible but not suggested          */
    	// Casting IS necessary when working with this list
    	// There is no type safety
    	List list = new ArrayList();  // normally List<TYPE> myList = new ArrayList<TYPE>();
//    	list.add("hello");
//    	String s = (String) list.get(0);
//    	System.out.println(s);
    	
    	// We can add any object to this list
//    	list.add(1); 
//    	String s2 = (String) list.get(1);  // ClassCastException!
//    	System.out.println(s2);
    	// We run into a classcast exception because a list should 
    	// only hold one type. We need type safety!
    	
    	
    	// TYPE SAFETY
    	// Casting IS NOT necessary when working with this list,
    	// but it is suggested to use it for type security.
    	// We are using "generics" at this point. When looking at
    	// ArrayList.java (from the JDK) we will see the following:
    	//    public class ArrayList<E>
    	// Where "E" is our generic. (E stands for Element)
//		List<String> list = new ArrayList<String>();
//		list.add("hello");
//		list.add("1");				
//		String s = list.get(0);
//		String s2 = list.get(1);
//		System.out.println(s);
//		System.out.println(s2);
		
    	
    	/**  3.	Enables programmers to implement generic algorithms    */
    	
    	/**
    	 * Generally the naming conventions for Generics is as follows:
    	 *    E - Element (will appear a lot in Java's library)
    	 *    K - Key
    	 *    N - Number
    	 *    T - Type
    	 *    V - Value
    	 *    S,U,V etc. - 2nd, 3rd, 4th types 
    	 */    	
    	
    	/** Test MyGenericClass with <Animal> */
    	
    	// Review MyGenericClass.java
    	
    	// Instantiate the generic class and set the "rule" that 
    	// the "type" will be of type "Animal" (or a subclass of Animal).
        MyGenericClass<Animal> myClass = new MyGenericClass<Animal>();
        Cat cat = new Cat("Tabby", "RebeccaPurple");
        
        // Set the object "cat" on myClass (works since it abides by the 
        // generic we defined earlier)
        myClass.set(cat);
        
        // Get the object (java.lang.Object) back and print it
        Object result = myClass.get();
//        System.out.println("result=" + result);
//        System.out.println("result instanceof Object: " + (result instanceof Object));
//        System.out.println("result instanceof Animal: " + (result instanceof Animal));
//        System.out.println("result instanceof Cat: " + (result instanceof Cat));
        
        // Can't call Animal/Cat functions on the result 
        // that was declared
//        System.out.println(result.sound()); // compilation error
        
        // We can typecast the object back to Animal if we want.
        // Now we can access the subclass and superclass functions.
        Cat tabby = (Cat)myClass.get();
        System.out.println(tabby.sound());
    }
    
    private void testGenericClass2() {
        /** Test MyGenericClass with <Integer> */
    	
    	// Use an Integer this time and set the "object" to 0
        MyGenericClass<Integer> test = new MyGenericClass<Integer>();
        test.set(0);
        
        // Is the Object returned equal to the number zero?
//        if (test.get() == 0) {
//            System.out.println("object returned from getter == 0");
//        } else {
//        	System.out.println("object returned from getter != 0");
//        }
        
        // Prove objects can == each other (not .equals() each other)
        Object o = 1;        
        Integer i = 1;
        
        // Are the values of o and i evaluated to be the same?                
//        System.out.println("o==i: " + (o==i)); 
        
        // Are the properties assessed in the objects evaluated
        // to be the same?
//        System.out.println("o.equals(i): " + o.equals(i));
        
        // The value for int and Integer are evaluated to be the same
        Integer integer = 1;
        int y = 1;
//        System.out.println("integer==y: " + (integer==y)); 
        
        // Right...
//        Integer n1 = 1;
//        Integer n2 = (Integer.valueOf(String.valueOf("1"))* 100) / 100;
//        System.out.println("n1 instanceof Integer: " + (n1 instanceof Integer));
//        System.out.println("n2 instanceof Integer: " + (n2 instanceof Integer));
//        System.out.println("System.identityHashCode(n1): " + System.identityHashCode(n1));
//        System.out.println("System.identityHashCode(n2): " + System.identityHashCode(n2));
//        System.out.println("n1 == n2: " + (n1 == n2));
//        System.out.println();
//        
//        // AHHH!!!! 
//        Integer n3 = new Integer(1);
//        Integer n4 = new Integer(1);
//        System.out.println("n3 instanceof Integer: " + (n3 instanceof Integer));
//        System.out.println("n4 instanceof Integer: " + (n4 instanceof Integer));
//        System.out.println("System.identityHashCode(n3): " + System.identityHashCode(n3));
//        System.out.println("System.identityHashCode(n4): " + System.identityHashCode(n4));
//        System.out.println("n3 == n4: " + (n3 == n4));
//             
//        // THE FIX!!
//        Integer n5 = new Integer(1);
//        Integer n6 = new Integer(1);
//        System.out.println("n5 == n6: " + (n5.intValue() == n6.intValue()));
        
        // Test custom Objects
        A1 a1 = new A1();
        A2 a2 = new A2();
//        System.out.println(a1==a2); // doesn't compile
//        System.out.println("a1.equals(a2): " + a1.equals(a2));
    }

    /**
     * I love maps so much that I created my own!
     * David McCauley wait... no... Jeffery  .... 9/29/2021
     */
    private void testGenericClass3() {
        /** Test the Generic Type Class "Pair" */
    	
    	// Instantiate the new class using a Generic that 
    	// takes two parameters key (K) and value (V). 
        Pair<Integer, String> pair = new Pair<Integer, String>(0, "Cat");
        System.out.println("pair:" + pair);
        
        // Instantiate the class again passing a list as the value 
        Pair<String, List<Integer>> pair2 = new Pair<>("Cat", Arrays.asList(0,2,4));
        System.out.println("pair2:" + pair2);
    }

    private void testGenericClass4() {
    	// We can bind the generic parameter to another object
    	// MyGenericClass2 binds "T" to Integer using
    	//    <T extends Integer>
    	
        // Bound mismatch: The type Animal is not a valid substitute 
        // for the bounded parameter <T extends Integer> of the type 
        // MyGenericClass2<T>
//        MyGenericClass2<Animal> myClass = new MyGenericClass2<>(); 
        // compilation error
                
        // Note: the review game and/or milestone asks if we can 
        //       bind a generic. The answer is yes. (T extends Integer)
        
        /** Let's see a working example of a "bound generic"  */
    	/** MyGenericClass3 <T extends Animal>                */
    	
        // This should work right?
        MyGenericClass3<Animal> myClass2 = new MyGenericClass3<Animal>(); 
    	
    	// Will this work?
//    	MyGenericClass3<Cat> myClass = new MyGenericClass3<Cat>(); 
    }
    
    private void testWildcard() {
    	/** 
    	 * The wildcard type parameter <?> is used in the case 
    	 * where you want to be able to have methods receive a
    	 * list as a parameter containing any subclass of Object.
    	 * 
    	 * for example:   public void printList(List<?> internList) {}
    	 * 
    	 * The method becomes more flexible as it can process lists
    	 * of unknown types (no need for multiple methods or
    	 * overloading).
    	 */
    	
        // We cannot "instantiate" a list with a wildcard
    	// Meaning: the code following the keyword "new"
//        List<?> myList = new ArrayList<?>(); // Compilation Error
 
    	
//        List<Object> myList = new ArrayList<Object>(); // Compilation Error??
        
    	
        /** Wildcard test with <Animal> */       
//        List<Animal> animalList = new ArrayList<Animal>(); 
//        Cat cat = new Cat(Animal.TYPE_FELINE, Animal.COLOR_PURPLE); 
//        Dog dog = new Dog(Animal.TYPE_CANINE, Animal.COLOR_WHITE); 
//        animalList.add(cat); 
//        animalList.add(dog); 
//        Helper.printWildList(animalList);    	
    	
        
        /** Wildcard test with <Integer> */
//        List<Integer> integerList = new ArrayList<Integer>();
//        integerList.add(1);
//        integerList.add(2);
//        Helper.printWildList(integerList);
        
        /** 
         * Not a wildcard example (because we are not using ?, 
         * but shows how we can use "Object"
         * to handle lists of unknown types.
         */
        List<Object> objectList = new ArrayList<Object>();
        objectList.add(new BigDecimal("0.00")); 
        objectList.add(new Integer(2)); 
        objectList.add(1);
        Helper.printObjectList(objectList);
    	
        /**
         * Will this compile? 
         */
//		List<Animal> animalList2 = new ArrayList<Animal>();
//		Cat cat = new Cat(Animal.TYPE_FELINE, Animal.COLOR_PURPLE);
//		Dog dog = new Dog(Animal.TYPE_CANINE, Animal.COLOR_WHITE);
//		animalList2.add(cat);
//		animalList2.add(dog);
//		Helper.printObjectList(animalList2);

    }
    
    /**
     * - When you add text to the Exception, it becomes the "message"
     * - When you throw it but remove the text ArithmeticException() 
     *   the message is empty.
     * - When you do not throw the exception, the default message 
     *   shows:  / by zero 
     *      
     *   ArithmeticException is a RuntimeException (extends RuntimeException)
     *   This means we do not need to place "throws ArithmeticException"
     *   after the method signature.
     * @param a
     * @param b
     * @return
     */
    private int divide(int a, int b) { // throws ArithmeticException {
        if (b == 0) { 
            throw new ArithmeticException("My Custom Division by Zero message");        	
        } else { 
           return a / b; 
        }         
     }
    
    private void checkedException() throws IOException {
        FileInputStream fis = null;
            
        try {
            fis = new FileInputStream("C:/someFileThatDoesNotExist.txt");
        } finally {
            try {fis.close();} catch (Exception e) {/* do nothing */}
        }
    }
    
    private void checkedException2() throws IOException {
        FileInputStream fis = null;
            
        try {
            fis = new FileInputStream("C:/aFile.txt"); // create this file in your file system
        } catch(IOException ioe) { 
            ioe.printStackTrace();
            System.out.println("[checkedException2] I/O Exception getting file. ioe.getMessage() =>" + ioe.getMessage());
            throw ioe;
        } finally {
        	System.out.println("Foo");
            try {fis.close();} catch (Exception e) {/* do nothing */}
        }
    }
    
    // JDK8+
    private static void testFileInputReaderWithTry() throws IOException {
        try (FileInputStream fis = new FileInputStream("/temp/file.txt")) {
            int data = fis.read();
            while (data != -1) {
                System.out.print((char) data);
                data = fis.read();
            }
        }
    }

    
    private void testCheckedExceptions() {  	
    	
    	/** Checked Exception 1 */
//        try {
//            checkedException();
//        } catch (IOException ioe) {
//        	// Prints the stacktrace to the console.
//        	System.out.println("Stacktrace 1:");
//            ioe.printStackTrace();
//            
//            // Prints a custom message to the console. (w/o a stacktrace)
//            System.out.println("\nMessage:");
//            System.out.println("I/O Exception getting file. ioe.getMessage() =>" + 
//            		ioe.getMessage() + "\n");
//            
//            // Cool way to retrieve the full stack exception
//            System.out.println("Stacktrace 2:");
//            String stackTrace = Helper.getErrors(ioe);
//            System.out.println(stackTrace);
//        }
        
        /** Checked Exception 2                 
         * Shows how an exception is thrown
         * View the method "checkedException2()" to see where 
         * the exception is being thrown.  
         * 
         * No error is actually thrown if the file C:\aFile.txt
         * exists in your file system.
         */
//        try {
//        	checkedException2();
//        } catch (IOException ioe) {
//        	ioe.printStackTrace();
//        	System.out.println("IO Exception was caught!");
//        } catch (Exception e) {
//        	e.printStackTrace();
//        	System.out.println("General Exception was caught!");
//        }
        
        
        /** Test File Input Reader With Try                       */
        /** Make sure /temp/file.txt exists on your file system!  */
//        try {
//            testFileInputReaderWithTry();    
//        } catch (IOException ioe) {
//            ioe.printStackTrace();
//            System.out.println("I/O Exception getting file. ioe.getMessage() =>" + ioe.getMessage());
//        }
    	
    } // end testCheckedExceptions()
   
    /**
     * This is on the milestone. Know how to create a custom
     * exception from scratch.
     * 
     * public class CustomException extends Exception {
     *      // Pass message and generated "cause" to the super class Exception
     * 	    public CustomException(String message) {
     * 	        super(message, new Throwable(message));
     * 	    }
     * 	    	
     * 	    // Pass message and cause to the super class Exception
     * 	    public CustomException(String message, Throwable cause) {
     * 	        super(message, cause);
     * 	    }
     * }
     * 
     */
    private void testCustomExceptions() {
        try {
            checkAge(20);    
        } catch (UnderageException uae) {
            System.out.println("Underage Error. uae.getMessage() => " + uae.getMessage());
        }
    }
   
    /** UnderageError extends Exception and Exception extends Throwable */
    /** The messages passed into the UnderageError class are passed to  */
    /** the two super classes.                                           */
    public void checkAge(int age) throws UnderageException{
        if (age < 21) {
            throw new UnderageException("User is underage!");
        }  
    }
    
    private void printList(List<String> list) {
        for (String string : list) {
            System.out.println(string);
        }    
    }

    private void printListSafely(List<String> list) {
    	if (list != null) { // null pointer check
            for (String string : list) {
                System.out.println(string);
            }
    	} 
    }
    
    private void testRuntimeExceptions() {
       	
        /**
         * Test division by zero
         * div(1,0); 
         * simple example
         * I am passing in my own message! Check the divide method!
         */
//    	divide(1, 0);
    	
    	
		/** Test Empty List */
    	// notice that when we uncomment the next block of code
    	// and run printList, the method "printListSafely" is 
    	// never executed. After an exception is thrown and not
    	// caught, execution stops.
//		List<String> myList = null;
//		printList(myList); // throws NPE
		
		// Correct way to handle printing
//		List<String> myList2 = null;
//		printListSafely(myList2); // no print & no NPE        	
    	
    	/** Fun Fact */
    	// Arrays.asList creates the list with the List Interface and
    	// not with the List implementation (ArrayList). Any method in
    	// ArrayList that you would normally call, is no longer available
    	// to you.
//    	System.out.println(Arrays.asList(
//    			 new String[] {"1", "2", "3"}) instanceof ArrayList);
//    	
//    	System.out.println(Arrays.asList(
//   			 new String[] {"1", "2", "3"}) instanceof List);
        
    	// A method on ArrayList.java is "trimToSize()" which trims the
    	// capacity on the list. Let's try to call it.
//    	Arrays.asList(new String[] {"1", "2", "3"}).trimToSize();
    	
    	/** Test Runtime Exception */
    	// Is this another fix for ensuring we do not run into an exception?
//    	List<String> myList = null;
//    	testRuntimeException(myList);
    	
    }
    
    private void testRuntimeException(List<String> list) {
        if (list.size() >= 0) {
            for (String string : list) {
                System.out.println(string);
            }    
        } 
    }


	/**
     * - Using next() will only return what comes before a space. 
     * - nextLine() automatically moves the scanner down after returning 
     *   the current line.
     * - A useful tool for parsing data from nextLine() would be 
     *     str.split("\\s+").
     */
    private void testScanner() {   
        Scanner scanner = null;
        
        /** scanner.next() */
//        try {
//        	// We are using the scanner method "next()"
//        	// In this case, the first space is replaced 
//        	// with an empty string and everything after 
//        	// the first space is ignored.
//            System.out.println("Enter your full name: ");
//            scanner = new Scanner(System.in);
//            String username = scanner.next();
//            System.out.println("Your full name is " + username);
//        } finally {
//            scanner.close();
//        }
        
        /** 
         * scanner.next()  scanner.next() ???? 
         * Let's arbitrarily grab the next token and see if we get
         * the last name...
         * If you only enter your first name and press enter, the
         * scanner will wait for the next token!
         */
//        try {
//        	// We are using the scanner method "next()"
//        	// In this case, the first space is replaced 
//        	// with an empty string and everything after 
//        	// the first space is ignored.
//            System.out.println("Enter your full name: ");
//            scanner = new Scanner(System.in);
//            String username = scanner.next();
//            System.out.println("Your full name is " + username + " " + scanner.next());
//        } finally {
//            scanner.close();
//        }
        
        
    	/** 
    	 * Let's use scanner's nextLine() method instead.
    	 * scanner.nextLine() 
    	 */
//        try {
//        	// All text is "captured"
//            System.out.println("Enter your first and last name: ");
//            scanner = new Scanner(System.in);
//            String username = scanner.nextLine();
//            System.out.println("Your full name is " + username);
//        } finally {
//            scanner.close();
//        }
        
        /** 
         * Java 8 - AutoCloseable Interface
         * There is no need to close the scanner when you are
         * finished with it
         */
//        try (Scanner scanner2 = new Scanner(System.in)) {
//            System.out.println("Enter your username: ");
//            String username = scanner2.nextLine();
//            System.out.println("Your username is " + username);
//        }
        
    }
    

    /**
     *  For distributed applications, do not reference files in 
     *  your local file system!!
     *   	
     *	This is used for demonstration purposes only!!
     *   	
     *	DO NOT REFERENCE THE LOCAL FILE SYSTEM IN YOUR LABS OR FUTURE WEB APPLICATIONS 
     */
    private void testFileReader() {
        BufferedReader br = null;
        FileReader fr = null;
        
        /** JDK 7 */
//        try {
//            // DO NOT REFERENCE THE LOCAL FILE SYSTEM IN YOUR 
//        	// LABS OR FUTURE WEB APPLICATIONS. THIS IS A DEMONSTRATION. 
//            File file = new File("/temp/file5.txt");
//            
//            if (!file.exists()) {
//            	System.out.println("file not found... creating file...");
//            	file.createNewFile();
//            	System.out.println("file created.");
//            }
//            
//            fr = new FileReader(file);
//            br = new BufferedReader(fr);
//            String input;
//        
//            while ((input = br.readLine()) != null) {            	
//            	// Split a comma separated string that contains double quotes
//            	// Since the RegEx targets double quotes "between" values, we
//            	// need to remove the double quote at the start of the entry
//            	// so that the RegEx executes properly.
//                String[] values = input.replaceAll("^\"", "")
//                		.split("\"?(,|$)(?=(([^\"]*\"){2})*[^\"]*$) *\"?");
//            	
//                for (String string : values) {
//                    System.out.println(string);
//                }
//            }
//            
//        } catch(Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                br.close();
//            } catch (Exception e) {/*do nothing*/}
//            try {
//                fr.close();
//            } catch (Exception e) {/*do nothing*/}
//        }
 

        /** 
         * JDK 8
         * Using the AutoCloseable interface to close the BufferedReader 
         */ 
//        try {
//        	// For distributed application, do not reference local file system
//    		// This is used for demonstration purposes
//        	// DO NOT REFERENCE THE LOCAL FILE SYSTEM IN YOUR LABS OR FUTURE WEB APPLICATIONS
//            try (BufferedReader br2 = new BufferedReader(new FileReader(new File("/temp/file5.txt")))) {
//                String st;
//
//                while ((st = br2.readLine()) != null) {
//                    System.out.println(st);
//                }
//            }
//        } catch (Exception e) {
//            /*do nothing */
//            e.printStackTrace();
//        }
        

    }
    
    private void quickTests() {
        // On-the-fly tests
        
    }

    public static void main(String[] args) {
        new W5D3TestHarness();
    }

}
