package com.oreillyauto.java.week1.day01;

public class ClockReader {
    
    Clock clock = new Clock();

    public long readClock(){
        return clock.time;
    }
    
    public long getClockAndTime(){
        return this.getClock().time;
        //return this.clock.time;
        //return this.readClock();
    }

    public Clock getClock() {
        return clock;
    }
    
    /*
    public static void main(String[] args) {
		ClockReader cr = new ClockReader();
		System.out.println(cr.readClock());
	}
	*/
    
}
