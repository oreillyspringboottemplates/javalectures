package com.oreillyauto.java.week1.day04.example2.Books;

import java.util.Date;

public class PenDragon extends Book {
	@SuppressWarnings("deprecation")
	public PenDragon() {
		this.setAuthor("DJ McHale");
		this.setReleaseDate(new Date(2002, 9, 1));
		this.setTitle("Pendragon");
		this.setPages(372);
	}
	
	
	@Override
	public String smellIt() {
		return "Nostalgic!";
	}
}