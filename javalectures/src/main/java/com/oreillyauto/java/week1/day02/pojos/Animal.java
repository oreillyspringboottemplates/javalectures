package com.oreillyauto.java.week1.day02.pojos;

public class Animal {
    
    public final static String COLOR_BLACK = "BLACK";
    public final static String COLOR_PURPLE = "PURPLE";
    public final static String COLOR_RED = "RED";
    public final static String COLOR_WHITE = "WHITE";
    
    public final static String TYPE_FELINE = "FELINE";
    public final static String TYPE_CANINE = "CANINE";
    
    public String type = "";
    public String color = "";
    
    public Animal() {}

    public String sound() {
    	return "meh";
    }
    
    protected String habitat() {
    	return "world";
    }
    
    public Animal(String type) {
        this.type = type;
    }

    public Animal(String type, String color) {
        this.type = type;
        this.color = color;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }




    /*
    @Override
    public String toString() {
        return "Animal [type=" + type + ", color=" + color + "]";
    }
    */
 
 
    /**
     * A hashcode is an integer value associated with every object in Java, 
     * facilitating the hashing in hash tables. ... The hashcode() method 
     * returns the same hash value when called on two objects, which are 
     * equal according to the equals() method. And if the objects are unequal, 
     * it usually returns different hash values.
     */    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	public String getAnimalSound() {
        return "MEH!";
    }
}
