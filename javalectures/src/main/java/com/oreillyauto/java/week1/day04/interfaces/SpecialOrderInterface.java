package com.oreillyauto.java.week1.day04.interfaces;

public interface SpecialOrderInterface {
	public void showSpecialOrderId(int id);
}
