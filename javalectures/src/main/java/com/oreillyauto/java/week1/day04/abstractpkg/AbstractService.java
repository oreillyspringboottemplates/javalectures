package com.oreillyauto.java.week1.day04.abstractpkg;

public abstract class AbstractService extends AbstractJob {

    public abstract void doStuff();
    
    public void actuallyDoSomething() {
        System.out.println("Done");
    }
}
