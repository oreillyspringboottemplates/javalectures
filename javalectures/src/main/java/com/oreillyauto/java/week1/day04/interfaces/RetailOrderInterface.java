package com.oreillyauto.java.week1.day04.interfaces;

public interface RetailOrderInterface extends GenericOrderInterface, SpecialOrderInterface {
	String ORDER_TYPE = "Retail"; // automatically public, static, final
	public void showStoreNumber(int storeNumber);
	
	@Override // an interface overloading a default method from another interface
	public default void showDate() {
		System.out.println("hi, I'm the new sheriff in town");
	}

}
