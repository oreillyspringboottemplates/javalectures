package com.oreillyauto.java.week1.day03.enums;

import org.apache.commons.text.WordUtils;

public enum Month {
	// Each Enum value is "public static final" (implied) 
	JANUARY("Q1", "WINTER"),
    FEBRUARY("Q1", "WINTER"),
    MARCH("Q1", "SPRING"),
    APRIL("Q2", "SPRING"),
    MAY("Q2", "SPRING"),
    JUNE("Q2", "SUMMER"),
    JULY("Q3", "SUMMER"),
    AUGUST("Q3", "SUMMER"),
    SEPTEMBER("Q3", "FALL"),
    OCTOBER("Q4", "FALL"),
    NOVEMBER("Q4", "FALL"),
    DECEMBER("Q4", "WINTER");
	
    private final String quarter;
    private final String season;
    
    Month(String quarter, String season) {
        this.quarter = quarter;
        this.season  = season;
    }
    
    /**
     * This method uses the WordUtils library to fully capitalize words.
     * Converts all the whitespace separated words in a String into 
     * capitalized words,that is each word is made up of a titlecase 
     * character and then a series oflowercase characters.
     * @return
     */
	public String getQuarterAndSeason() {
    	return this.quarter + " " + WordUtils.capitalizeFully(this.season);
    }
    
    public String getQuarter() {
    	return quarter;
    }
    
    public String getSeason() {
    	return season;
    }
}
