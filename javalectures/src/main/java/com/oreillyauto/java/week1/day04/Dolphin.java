package com.oreillyauto.java.week1.day04;

class Dolphin extends Mammal {
	int debug = 1;
    private String myString;
    
    /**
     *  NOTE:
     *  Remember that when you decide to add super(); to a child class the
     *  (super) constructor call must be the first statement in a constructor
     *  
     *  Good:
     *    Dolphin() {
     *      super();
     *      dolphinCount+=1;
     *    }
     *    
     *  Bad:
     *    Dolphin() {
     *      dolphinCount+=1;
     *      super();
     *    }    
     */
    Dolphin() {
    	if (debug > 0 ) System.out.println("Dolphin empty constructor called.");
    }
    
    Dolphin(String str){
        myString = str;
        
        if (debug > 0 ) {
        	System.out.println("Dolphin Message from first constructor: " + myString);
        }
    }
    
    @Override
    public String animalTime() {
        return "It's dolphin time!";
    }

    public void printConstructorMessage() {
    	System.out.println(myString);
    }
    
    // Not required since the superclass Mammal has invokeSuperTime()
//    public String invokeSuperTime() {
//        return super.animalTime();
//    }
}
