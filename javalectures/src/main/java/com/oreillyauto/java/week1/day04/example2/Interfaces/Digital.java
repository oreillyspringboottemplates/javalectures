package com.oreillyauto.java.week1.day04.example2.Interfaces;

public interface Digital {
	String watchIt();
	String downloadFromShadyWebsite();
	boolean hasBeenWatchedByIsp();
}
