package com.oreillyauto.java.week1.day04.example2;

import java.util.Date;

import com.google.gson.Gson;

public abstract class Media {

	private String creator = "God";
	private String title = "";
	private Date releaseDate = new Date();

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

}
