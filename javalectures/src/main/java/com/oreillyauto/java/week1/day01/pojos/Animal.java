package com.oreillyauto.java.week1.day01.pojos;

public class Animal {
    
    public final static String COLOR_BLACK = "BLACK";
    public final static String COLOR_PURPLE = "PURPLE";
    public final static String COLOR_RED = "RED";
    public final static String COLOR_WHITE = "WHITE";
    
    public final static String TYPE_FELINE = "FELINE";
    public final static String TYPE_CANINE = "CANINE";
    
    public String type = "";
    public String color = "";
    
    public Animal() {}

    public void hide(String message) {
    	System.out.println("The hide method in Animal was "
    			+ "called with message: " + message);
    }    
    
    public void hide() {
    	System.out.println("The hide method in Animal was called!");
    }
    
    public void override() {
    	System.out.println("The override method in Animal was called!");
    }
    
    public String sound() {
    	return "meh";
    }
    
    protected String habitat() {
    	return "world";
    }
    
    public Animal(String type) {
        this.type = type;
    }

    public Animal(String type, String color) {
        this.type = type;
        this.color = color;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)   // this is equal to current Animal
            return true;
        if (obj == null)   // object that is passed in
            return false;
        if (getClass() != obj.getClass())  // do the classes even match?
            return false;
        Animal other = (Animal) obj;  // type casting
        if (type == null) {
            if (other.type != null)   // are both types null?
                return false;
        } else if (!type.equals(other.type)) // are both types equal?
            return false;
            
        // optional: add comparison of other member variables here
        
        return true; // yay!
    }

    @Override
    public String toString() {
        return "Animal [type=" + type + ", color=" + color + "]";
    }
 
}
