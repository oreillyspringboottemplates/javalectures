package com.oreillyauto.java.helpers;

public class StringHelper {

	public static String reverseString(String string) {
		if ("".equals(string)) {
			return null;
		} else if (string == null) {
			return null;
		} else {
			return new StringBuilder().append(string).reverse().toString();
		}
	}

}
