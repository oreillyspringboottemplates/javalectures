package com.oreillyauto.java.helpers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class Helper {
        
	public static void printFoo() {
		System.out.println(FooHelper.getFoo());
	}
	
	public static void printObjectList(List<Object> objectList) { 
	    for (Object object : objectList) { 
	        System.out.println(object); 
	    }         
	}
	
	public static void printWildList(List<?> wildList) {
	    for (Object object : wildList) {
	        System.out.println(new Gson().toJson(object));
	    }
	}

	public static String getErrors(Exception e) {
		StackTraceElement[] errorArray = e.getStackTrace();
		
		StringBuilder sb = new StringBuilder();
		
		for (StackTraceElement stackTraceElement : errorArray) {
			sb.append(sb.toString().length() > 0 ? 
					("\n"+stackTraceElement) : stackTraceElement);
		}
		
		return sb.toString();
	}

	public static void println(String message) {
		String clazz = "Unknown";
		String method = "Unknown";
		
		try {
	        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
	        
	        for (int i=1; i<stElements.length; i++) {
	            StackTraceElement ste = stElements[i];
	            
	            if (!ste.getClassName().equals(Helper.class.getName()) && 
	            		ste.getClassName().indexOf("java.lang.Thread")!=0) {
	                clazz = (ste.getClassName() == null) ? 
	                		"Unknown" : ste.getClassName();
	                method = (ste.getMethodName() == null) ? 
	                		"Unknown" : ste.getMethodName();
	            }
	        }			
		} catch (Exception e) {/* do nothing */}

		System.out.println("[" + clazz + ":"+method+"] " + message);
	}
	
	public static class FooHelper {
		public static String getFoo() {
			return "Foo";
		}
	}
	
	public static Integer getArrayListCapacity(ArrayList<Object> list) {
		try {
			// Reflection: getting an unknown field on the ArrayList Class
			Field field = ArrayList.class.getDeclaredField("elementData");
			field.setAccessible(true);
			final Object[] elementData = (Object[]) field.get(list);
			int length = elementData.length;
			return length;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void printArray(Object[] array, String delimiter) {
		if (array != null && array.length > 0) {
			for (Object object : array) {
				System.out.print(object + delimiter);
			}
			
			System.out.println();
		}	
	}
	
	public static void printArray(int[] array, String delimiter) {
		if (array != null && array.length > 0) {
			for (Object object : array) {
				System.out.print(object + delimiter);
			}
			
			System.out.println();
		}	
	}

}
