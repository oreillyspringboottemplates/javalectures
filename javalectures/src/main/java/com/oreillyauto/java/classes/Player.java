package com.oreillyauto.java.classes;

public class Player implements Comparable<Player>  {
    
    private String name;
    private Integer ranking;
    
    public Player(String name, Integer ranking) {
        this.name = name;
        this.ranking = ranking;
    }   
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((ranking == null) ? 0 : ranking.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Player other = (Player) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (ranking == null) {
            if (other.ranking != null)
                return false;
        } else if (!ranking.equals(other.ranking))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Player [name=" + name + ", ranking=" + ranking + "]";
    }

    /**
     * CompareTo For String
     */
    @Override
    public int compareTo(Player o) {
        return this.getName().compareTo(o.getName());
    }

    /**
     * CompareTo For Integer
     */
//    @Override
//    public int compareTo(Player otherPlayer) {
//        return (this.getRanking() - otherPlayer.getRanking());
//    }
}